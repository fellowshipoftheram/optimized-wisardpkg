.PHONY: wisardpkg/wisardpkg.so test.o
wisardpkg/wisardpkg.so:
	g++ -fPIC -shared wrapper/wisardpkg.cc -o wisardpkg/wisardpkg.so -Wall -O3 -std=c++11

test: wisardpkg/wisardpkg.so
	python3 -m wisardpkg.test

all: wisardpkg/wisardpkg.so

test.o:
	g++ src/test.cc -g -O1 -o test.o

valgrind: test.o
	valgrind --leak-check=full --show-leak-kinds=all --track-origins=yes ./test.o