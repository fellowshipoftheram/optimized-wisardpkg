
class Bleaching: public ClassificationMethod {
public:
  Bleaching():bleachingActivated(true),confidence(1){}
  Bleaching(const bool bleachingActivated)
    :bleachingActivated(bleachingActivated),confidence(1){}
  Bleaching(const int confidence)
    :bleachingActivated(true),confidence(confidence){}

  int run(std::vector<std::vector<float>>& allvotes) const {
    std::vector<int> labels(allvotes.size());
    int outputLabel=0;
    float oldBleaching = 0;
    float bleaching = 0;

    while(true){
      oldBleaching = bleaching;
      bleaching = makeCut(labels,bleaching,allvotes);

      if(oldBleaching==bleaching) break;

      // ambiguity test
      int biggest = 0;
      int sbiggest = 0;

      for(unsigned int label=0; label<labels.size(); label++){
        if(labels[label] > biggest){
          sbiggest = biggest;
          biggest = labels[label];
          outputLabel=label;
        }
        else if(labels[label]>sbiggest){
          sbiggest=labels[label];
        }
      }
      // end of test
      if(!bleachingActivated) break;
      if(!((biggest-sbiggest) < confidence && biggest>confidence)) break;
    }
    return outputLabel;
  }


  virtual void predict(float* pred, std::vector<std::vector<float>>& allvotes) const {
    std::vector<int> labels(allvotes.size());
    // int outputLabel=0;
    float oldBleaching = 0;
    float bleaching = 0;

    while(true){
      oldBleaching = bleaching;
      bleaching = makeCut(labels,bleaching,allvotes);

      if(oldBleaching==bleaching) break;

      // ambiguity test
      int biggest = 0;
      int sbiggest = 0;

      for(unsigned int label=0; label!=labels.size(); label++){
        if(labels[label] > biggest){
          sbiggest = biggest;
          biggest = labels[label];
          // outputLabel=label;
        }
        else if(labels[label]>sbiggest){
          sbiggest=labels[label];
        }
      }
      // end of test
      if(!bleachingActivated) break;
      if(!((biggest-sbiggest) < confidence && biggest>confidence)) break;
    }
    float total = 0;
    int min = -1;
    for(uint32_t i=0; i!=labels.size(); i++) {
      if(min==-1 || labels[i] < min)
        min = labels[i];
    }
    for(uint32_t i=0; i!=labels.size(); i++){
      total += labels[i]-min;
    }
    if(total!=0)
      for(uint32_t i=0; i!=labels.size(); i++) pred[i] = (labels[i]-min)/total;
  }

  inline float makeCut(std::vector<int>& labels, float bleaching, std::vector<std::vector<float>>& allvotes) const {
      float min=1;
      for(unsigned int label=0; label!=allvotes.size(); label++){
        labels[label] = 0;
        for(unsigned int ram=0; ram!=allvotes[label].size(); ram++){
          if(allvotes[label][ram] > bleaching){
            labels[label]++;
            if(allvotes[label][ram] < min){
              min = allvotes[label][ram];
            }
          }
        }
      }
      return min;
  }

  ClassificationMethod* clone() const {
    return new Bleaching(bleachingActivated,confidence);
  }

private:
  Bleaching(const bool bleachingActivated,const int confidence)
    :bleachingActivated(bleachingActivated),confidence(confidence){}

private:
  bool bleachingActivated;
  int confidence;
};
