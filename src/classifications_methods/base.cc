class ClassificationMethod {
public:
    virtual int run(std::vector<std::vector<float>>& allvotes) const = 0;
    virtual void predict(float* pred, std::vector<std::vector<float>>& allvotes) const = 0;
    virtual ~ClassificationMethod(){}
    virtual ClassificationMethod* clone() const = 0;
};