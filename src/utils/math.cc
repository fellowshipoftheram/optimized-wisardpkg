
namespace math
{

  template<typename T>
  double sum(std::vector<T> v){
    return std::accumulate(std::begin(v), std::end(v), 0.0);
  }

  template<typename T>
  double mean(std::vector<T> v){
    return sum(v) / v.size();
  }

  template<typename T>
  double stdev(std::vector<T> v){
    double accum = 0.0;
    double m = mean(v);
    
    for(T& elem : v) accum += (elem - m) * (elem - m);
    
    return std::sqrt(accum / v.size());
  }

  template<typename T>
  std::vector<T> ranges(T start, T stop, uint64_t num) {
    std::vector<T> values(num);
    T step = (stop - start) / num;
    T value = start;
    for (uint64_t i = 0; i != num; i++){
      values[i] = value;
      value += step;
    }

    return values;
  }

}
