class Mean {
public:
  virtual Mean* clone() const = 0;
  virtual double calculate(const std::vector<std::vector<double>>& outputRams) = 0;
  virtual ~Mean(){}
};