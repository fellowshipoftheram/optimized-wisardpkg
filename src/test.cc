#include "wisardpkg.h"

int main(){
    uint32_t length = 9;
    uint32_t tupleSize = 3;
    uint32_t samples = 9;

    float X[] = {
        10,7,6,0,3,2,1,1,0,
        10,7,5,8,0,2,1,3,1,
        10,8,7,0,0,0,1,0,0,
        10,6,5,4,0,3,2,4,0,
        10,8,7,0,1,0,4,3,1,
        0,2,3,1,5,7,9,10,8,
        2,0,1,0,0,8,7,10,9,
        1,3,1,3,1,7,6,9,8,
        0,1,2,3,0,8,7,10,9
    };

    float y[] = {
        1.0,
        1.0,
        1.0,
        1.0,
        1.0,
        0.0,
        0.0,
        0.0,
        0.0
    };

    float y2[] = {
        1.0,
        1.4,
        0.4,
        0.7,
        0.9,
        0.6,
        0.2,
        0.1,
        0.05
    };


    Binarizer* mth = new MeanThreshold();
    Wisard wsd(length, tupleSize, mth);
    wsd.train(X,y,samples);

    float* out = new float[samples]();
    wsd.classify(X, out, samples);
    for(uint32_t i=0; i<length; i++){
        if(out[i]!=y[i]){
            std::cout << "classification different" << std::endl;
        }
    }

    float* pred = new float[samples*2];
    wsd.predict(X,pred,samples);

    wsd.classify(X, out, samples);
    std::cout << "pred: ";
    for(uint32_t i=0; i<length; i++){
        std::cout << "["<< pred[i*2] << ", " << pred[i*2+1] << "] ";
    }
    std::cout << std::endl;

    RegressionWisard rew(length,tupleSize,mth);
    rew.setSteps(100);
    rew.train(X,y2,samples);
    rew.predict(X,out, samples);
    for(uint32_t i=0; i<length; i++){
        if(out[i]!=y2[i]){
            std::cout << "regression different: " << out[i] << " " << y2[i] << std::endl;
        }
    }

    delete mth;
    delete[] out;
    delete[] pred;
    return 0;
}