#include "base.h"
#include "version.h"

#include "utils/math.cc"
#include "utils/exceptions.cc"

#include "utils/bin.cc"

#include "binarizations/base.cc"
#include "binarizations/default.cc"
#include "binarizations/meanthreshold.cc"
#include "binarizations/thermometer.cc"

#include "classifications_methods/base.cc"
#include "classifications_methods/bleaching.cc"

#include "models/wisard/reversediscriminator.cc"
#include "models/wisard/wisard.cc"

#include "predictions_methods/base.cc"
#include "predictions_methods/means.cc"

#include "models/rew/rewmemory.cc"
#include "models/rew/rew.cc"