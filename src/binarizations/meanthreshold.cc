class MeanThreshold: public Binarizer {
public:
    MeanThreshold():bitsByValue(1){}

    MeanThreshold(int bitsByValue):bitsByValue(bitsByValue){}

    void transform(Bin& bin, const float* data, const uint64_t length) const {
        transform<float>(bin,data,length);
    }
    void transform(Bin& bin, const uint8_t* data, const uint64_t length) const {
        transform<uint8_t>(bin,data,length);
    }

    inline uint64_t getBinSize(const uint64_t length) const {
        return length*bitsByValue;
    }

    Binarizer* clone() const {
        return new MeanThreshold(bitsByValue); 
    }

private:
    template<typename T>
    inline void transform(Bin& bin, const T* data, const uint64_t length) const {
        bin.setSize(getBinSize(length));
        double mean = 0;
        for(uint32_t k=0; k<length; k++) mean += data[k];
        mean /= length;
        for(uint32_t k = 0; k < length; k++){
            int v = data[k] < mean ? 0 : 1;
            for(int i=0; i<bitsByValue; i++)
                bin.set(k*bitsByValue+i, v);
        }
    }

private:
    int bitsByValue;
};