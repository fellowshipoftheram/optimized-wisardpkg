class DefaultCut: public Binarizer {
public:
    DefaultCut(){}

    void transform(Bin& bin, const float* data, const uint64_t length) const {
        transform<float>(bin,data,length);
    }
    void transform(Bin& bin, const uint8_t* data, const uint64_t length) const {
        transform<uint8_t>(bin,data,length);
    }

    uint64_t getBinSize(const uint64_t length) const {
        return length;
    }

    Binarizer* clone() const {
        return new DefaultCut(); 
    }

private:
    template<typename T>
    inline void transform(Bin& bin, const T* data, const uint64_t length) const {
        bin.setSize(length);
        for(uint32_t k = 0; k < length; k++){
            bin.set(k, data[k] > 0 ? 1 : 0);
        }
    }
};