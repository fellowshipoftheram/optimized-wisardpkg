class Binarizer {
public:
  virtual void transform(Bin& bin, const float* data, const uint64_t length) const = 0;
  virtual void transform(Bin& bin, const uint8_t* data, const uint64_t length) const = 0;

  virtual uint64_t getBinSize(const uint64_t length) const = 0;
  virtual Binarizer* clone() const = 0;
  virtual ~Binarizer(){}
};