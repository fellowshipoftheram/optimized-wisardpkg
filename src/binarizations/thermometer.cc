class SimpleThermometer : public Binarizer {
public:
  SimpleThermometer(const uint64_t thermometerSize=2, const double minimum=0.0, const double maximum=0.0) : thermometerSize(thermometerSize){
    valueRanges = math::ranges(minimum, maximum, thermometerSize);
  }

  void transform(Bin& bin, const float* data, const uint64_t length) const {
    transform<float>(bin,data,length);
  }
  void transform(Bin& bin, const uint8_t* data, const uint64_t length) const {
    transform<uint8_t>(bin,data,length);
  }

  uint64_t getBinSize(const uint64_t length) const{
      return thermometerSize*length;
  };

  Binarizer* clone() const {
      return new SimpleThermometer(thermometerSize, valueRanges);
  };

private:
    SimpleThermometer(uint64_t thermometerSize, std::vector<double> valueRanges)
    :thermometerSize(thermometerSize), valueRanges(valueRanges){}

    template<typename T>
    inline void transform(Bin& bin, const T* data, const uint64_t length) const {
      bin.setSize(length*thermometerSize);
      int k = 0;
      for(uint64_t i = 0; i != length; i++){
        for (uint64_t j = 0; j != valueRanges.size(); j++){
          if (data[i] > valueRanges[j]){
            bin.set(k, 1);
          } else {
            break;
          }
          k++;
        }
      }
    }

private:
  uint64_t thermometerSize;
  std::vector<double> valueRanges;
};


class DynamicThermometer : public Binarizer {
public:
  DynamicThermometer(
    const std::vector<uint64_t>& thermometerSizes, 
    const std::vector<double>& minimum=std::vector<double>(), 
    const std::vector<double>& maximum=std::vector<double>()): thermometerSize(0) {
    valueRanges.resize(thermometerSizes.size());
    for (uint64_t i = 0; i != valueRanges.size(); i++){
      double min = minimum.size() > 0 ? minimum[i] : 0.0;
      double max = maximum.size() > 0 ? maximum[i] : 1.0;
      valueRanges[i] = math::ranges(min, max, thermometerSizes[i]);
      thermometerSize += thermometerSizes[i];
    }
  }
  void transform(Bin& bin, const float* data, const uint64_t length) const {
    transform<float>(bin,data,length);
  }
  void transform(Bin& bin, const uint8_t* data, const uint64_t length) const {
    transform<uint8_t>(bin,data,length);
  }

  uint64_t getBinSize(const uint64_t length) const{
    return thermometerSize*length;
  };

  Binarizer* clone() const {
    return new DynamicThermometer(thermometerSize, valueRanges);
  };

private:
  DynamicThermometer(uint64_t thermometerSize,
  std::vector<std::vector<double>> valueRanges)
  :thermometerSize(thermometerSize), valueRanges(valueRanges){}

  template<typename T>
  inline void transform(Bin& bin, const T* data, const uint64_t length) const {
    bin.setSize(length*thermometerSize);
    int k = 0;
    for(uint64_t i = 0; i != length; i++){
      for (uint64_t j = 0; j != valueRanges[i].size(); j++){
        if (data[i] > valueRanges[i][j]){
          bin.set(k, 1);
        } else {
          break;
        }
        k++;
      }
    }
  }

private:
  uint64_t thermometerSize;
  std::vector<std::vector<double>> valueRanges;
};

