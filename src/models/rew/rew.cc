class RegressionWisard {
public:
    RegressionWisard(uint64_t entrySize, uint32_t tupleSize, Binarizer* biner=NULL)
    :entrySize(entrySize),tupleSize(tupleSize),steps(0){

        if(tupleSize>64){
            throw Exception("too big tuple size, max value 64");
            exit(10);
        }

        binarizer = biner == NULL ? new DefaultCut() : biner->clone();
        memory = new RewMemory(binarizer->getBinSize(entrySize), tupleSize);
        mean = new SimpleMean();
    }

    ~RegressionWisard(){
        delete binarizer;
        delete mean;
        delete memory;
    }

    void train(float* X, float* y, const uint64_t length){
        train<float>(X,y,length);
    }
    void train(uint8_t* X, float* y, const uint64_t length){
        train<uint8_t>(X,y,length);
    }
    void predict(float* X, float* y, const uint64_t length){
        predict<float>(X,y,length);
    }
    void predict(uint8_t* X, float* y, const uint64_t length){
        predict<uint8_t>(X,y,length);
    }

    void setMean(Mean* m){
        delete mean;
        mean = m->clone();
    }

    void setSteps(const uint32_t s){
        steps = s;
    }

private:
    template<typename T>
    inline void train(T* X, float*y, const uint64_t length){
        std::vector<Bin> bins(length);
        for(uint64_t i=0; i<length; i++){
            binarizer->transform(bins[i],&X[i*entrySize], entrySize);
            memory->train(bins[i],y[i]);
        }

        for (uint32_t j = 0; j < steps; j++){
            for (size_t i = 0; i < length; i++){
                auto votes = memory->getVotes(bins[i]);
                float y_pred = mean->calculate(votes);
                memory->calculateFit(bins[i], (y[i] - y_pred));
            }
            memory->applyFit();
        }
    }

    template<typename T>
    inline void predict(T* X, float* y, const uint64_t length){
        for(uint64_t i=0; i<length; i++){
            Bin b;
            binarizer->transform(b,&X[i*entrySize], entrySize);
            auto votes = memory->getVotes(b);
            y[i] = mean->calculate(votes);
        }
    }

private:
    uint64_t entrySize;
    uint64_t tupleSize;
    uint32_t steps;
    Binarizer* binarizer;
    Mean* mean;
    RewMemory* memory;
};