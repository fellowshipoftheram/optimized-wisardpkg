typedef uint64_t rew_addr_t;
typedef std::vector<double> regression_content_t;
typedef std::unordered_map<rew_addr_t, regression_content_t> regression_ram_t;

class RewMemory {
public:
    RewMemory(uint32_t entrySize, uint32_t tupleSize)
    :tupleSize(tupleSize),entrySize(entrySize)
    {
        numberOfRAMS = entrySize / tupleSize;
        int remain = entrySize % tupleSize;
        int indexesSize = entrySize;
        if(remain > 0) {
            numberOfRAMS++;
            indexesSize += tupleSize-remain;
        }


        mapping.resize(indexesSize);
        for(uint32_t i = 0; i < entrySize; i++) {
            mapping[i]=i;
        }

        std::random_device rd;
        std::mt19937 mt(rd());
        std::uniform_int_distribution<uint32_t> dist(0, entrySize);

        for(uint32_t i = entrySize; i < mapping.size(); i++){
            mapping[i] = mapping[dist(mt)];
        }

        std::shuffle(mapping.begin(), mapping.end(), mt);
        memory = std::vector<regression_ram_t>(numberOfRAMS);
    }

    RewMemory(std::vector<uint32_t> mapping, uint32_t tupleSize, uint32_t entrySize)
    :tupleSize(tupleSize),entrySize(entrySize),mapping(mapping){
        numberOfRAMS = mapping.size()/tupleSize;
        memory = std::vector<regression_ram_t>(numberOfRAMS);
    }

    void train(const Bin& image, const double y) {

        for(uint32_t i=0; i<numberOfRAMS; i++){
            rew_addr_t index = getIndex(image,i);

            auto it = memory[i].find(index);
            if (it == memory[i].end())
                memory[i].insert(it, std::pair<rew_addr_t, regression_content_t>(index, {1, y, 0}));
            else{
                it->second[0]++;
                it->second[1] += y;
            }
        }
    }

    void setVotes(std::vector<regression_content_t>& votes, const Bin& image) const {
        for(uint32_t i=0; i<numberOfRAMS; i++){
            rew_addr_t index = getIndex(image,i);

            auto it = memory[i].find(index);
            if (it != memory[i].end()){
                votes[i][0] = it->second[0];
                votes[i][1] = it->second[1];
            }
        }
    }

    std::vector<regression_content_t> getVotes(const Bin& image){
        std::vector<regression_content_t> votes(numberOfRAMS, {0,0});
        setVotes(votes, image);
        return votes;
    }

    void calculateFit(const Bin& image, const double yFit) {
        for(uint32_t i=0; i<numberOfRAMS; i++){
            rew_addr_t index = getIndex(image,i);
            auto it = memory[i].find(index);
            it->second[2] += yFit;
        }
    }

    void applyFit() {
        for(uint32_t i=0; i<numberOfRAMS; i++){
            for (auto it = memory[i].begin(); it != memory[i].end(); ++it){
                it->second[1] += it->second[2] / it->second[0];
                it->second[2] = 0;
            }
        }
    }

private:
    inline rew_addr_t getIndex(const Bin& image, uint32_t ramId) const {
        rew_addr_t index = 0;
        uint32_t start = ramId*tupleSize;

        for(uint32_t i=start; i!=tupleSize+start; i++){
            uint32_t bin = image[mapping[i]];
            uint32_t j = i-start;
            index |= bin << j;
        }
        return index;
    }

private:
    uint32_t tupleSize;
    uint32_t numberOfRAMS;
    uint32_t entrySize;
    std::vector<uint32_t> mapping;

    std::vector<regression_ram_t> memory;
};