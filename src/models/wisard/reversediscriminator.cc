typedef uint64_t rd_addr_t;
typedef std::unordered_map<uint16_t,uint32_t> rd_content_t;
typedef std::unordered_map<rd_addr_t, rd_content_t> rd_ram_t;

class ReverseDiscriminator {
public:
    ReverseDiscriminator(uint32_t entrySize, uint32_t tupleSize)
    :tupleSize(tupleSize),entrySize(entrySize)
    {

        numberOfRAMS = entrySize / tupleSize;
        int remain = entrySize % tupleSize;
        int indexesSize = entrySize;
        if(remain > 0) {
            numberOfRAMS++;
            indexesSize += tupleSize-remain;
        }


        mapping.resize(indexesSize);
        for(uint32_t i = 0; i < entrySize; i++) {
            mapping[i]=i;
        }

        std::random_device rd;
        std::mt19937 mt(rd());
        std::uniform_int_distribution<uint32_t> dist(0, entrySize);

        for(uint32_t i = entrySize; i < mapping.size(); i++){
            mapping[i] = mapping[dist(mt)];
        }

        std::shuffle(mapping.begin(), mapping.end(), mt);
        positions = std::vector<rd_ram_t>(numberOfRAMS);
    }

    ReverseDiscriminator(std::vector<uint32_t> mapping, uint32_t tupleSize, uint32_t entrySize)
    :tupleSize(tupleSize),entrySize(entrySize),mapping(mapping){
        numberOfRAMS = mapping.size()/tupleSize;
        positions = std::vector<rd_ram_t>(numberOfRAMS);
    }

    void train(const Bin& image, uint16_t aClass){
        auto c = counter.find(aClass);
        if(c==counter.end())
            counter.insert(c, std::pair<uint16_t,uint64_t>(aClass,1));
        else
            c->second++;
        for(uint32_t i=0; i!=numberOfRAMS; i++){
            auto index = getIndex(image, i);
            
            auto it = positions[i].find(index);
            if(it == positions[i].end()){
                rd_content_t content;
                content[aClass]=1;
                positions[i].insert(it,std::pair<rd_addr_t,rd_content_t>(index, content));
            }
            else{
                auto itContent = it->second.find(aClass);
                if(itContent == it->second.end()){
                    it->second.insert(itContent, std::pair<uint16_t,uint32_t>(aClass,1));
                }
                else{
                    itContent->second++;
                }
            }
        }
    }

    void setVotes(std::vector<std::vector<float>>& votes, const Bin& image){
        for(uint32_t r=0; r!=numberOfRAMS; r++){
            auto index = getIndex(image, r);
            auto content = positions[r].find(index);
            if(content == positions[r].end()) continue;
            for(auto it : content->second){
                float a = (float)it.second/counter[it.first];
                votes[it.first][r] += a;
            }
        }
    }

    std::vector<std::vector<float>> getVotes(const Bin& image){
        std::vector<std::vector<float>> votes(counter.size(),std::vector<float>(numberOfRAMS,0));
        setVotes(votes, image);
        return votes;
    }

    // std::vector<std::vector<double>> getMentalImages(){
    //     std::vector<std::vector<double>> mis(counter.size(),std::vector<double>(entrySize,0));
    //     for(size_t ramId=0; ramId<positions.size(); ramId++){
    //         uint32_t start = ramId*tupleSize;
    //         for(auto pos: positions[ramId]){
    //             for(size_t i=0; i<tupleSize; i++){
    //                 char b = pos.first[i/8]&(1<<(i%8));
    //                 if(b==0) continue;
    //                 uint32_t addr = mapping[start+i];
    //                 for(auto d: pos.second){
    //                     mis[d.first][addr] += d.second;
    //                 }
    //             }
    //         }
    //     }
    //     return mis;
    // }

    // uint64_t getNumberOfTrainings(uint32_t aClass){
    //     return counter[aClass];
    // }

    // uint32_t getTotalRAMS() const {
    //     return numberOfRAMS;
    // }

    uint32_t getTotalClasses(){
        return counter.size();
    }

private:
    inline rd_addr_t getIndex(const Bin& image, uint32_t ramId) const {
        rd_addr_t index = 0;
        uint32_t start = ramId*tupleSize;

        for(uint32_t i=start; i!=tupleSize+start; i++){
            uint32_t bin = image[mapping[i]];
            uint32_t j = i-start;
            index |= bin << j;
        }
        return index;
    }

private:
    uint32_t tupleSize;
    uint32_t numberOfRAMS;
    uint32_t entrySize;
    std::vector<uint32_t> mapping;
    std::vector<rd_ram_t> positions;
    std::unordered_map<uint16_t,uint64_t> counter;
};