class Wisard {
public:
    Wisard(uint64_t entrySize, uint32_t tupleSize, Binarizer* biner=NULL)
    :entrySize(entrySize),tupleSize(tupleSize){

        if(tupleSize>64){
            throw Exception("too big tuple size, max value 64");
            exit(10);
        }
        
        binarizer = biner == NULL ? new DefaultCut() : biner->clone();
        classifcation_method = new Bleaching();
        d = new ReverseDiscriminator(binarizer->getBinSize(entrySize), tupleSize);
    }
    ~Wisard(){
        delete d;
        delete binarizer;
        delete classifcation_method;
    }

    void train(float* X, float* y, const uint64_t length){
        train<float>(X,y,length);
    }
    void train(uint8_t* X, float* y, const uint64_t length){
        train<uint8_t>(X,y,length);
    }
    void predict(float* X, float* y, const uint64_t length){
        predict<float>(X,y,length);
    }
    void predict(uint8_t* X, float* y, const uint64_t length){
        predict<uint8_t>(X,y,length);
    }
    void classify(float* X, float* y, const uint64_t length){
        classify<float>(X,y,length);
    }
    void classify(uint8_t* X, float* y, const uint64_t length){
        classify<uint8_t>(X,y,length);
    }

    void setClassificationMethod(ClassificationMethod* clas_mth){
        delete classifcation_method;
        classifcation_method = clas_mth->clone();
    }

    uint32_t getTotalClasses(){
        return d->getTotalClasses();
    }

private:
    template<typename T>
    inline void train(T* X, float*y, const uint64_t length){
        for(uint64_t i=0; i!=length; i++){
            Bin b;
            binarizer->transform(b,&X[i*entrySize], entrySize);
            d->train(b,y[i]);
        }
    }

    template<typename T>
    inline void predict(T* X, float* y, const uint64_t length){
        uint32_t c = d->getTotalClasses();
        for(uint64_t i=0; i!=length; i++){
            Bin b;
            binarizer->transform(b,&X[i*entrySize], entrySize);
            auto allvotes = d->getVotes(b);
            classifcation_method->predict(&y[i*c],allvotes);
        }
    }

    template<typename T>
    inline void classify(T* X, float* y, const uint64_t length){
        for(uint64_t i=0; i!=length; i++){
            Bin b;
            binarizer->transform(b,&X[i*entrySize], entrySize);
            auto allvotes = d->getVotes(b);
            y[i] = classifcation_method->run(allvotes);
        }
    }

private:
    ReverseDiscriminator* d;
    uint64_t entrySize;
    uint64_t tupleSize;
    ClassificationMethod* classifcation_method;
    Binarizer* binarizer;
};