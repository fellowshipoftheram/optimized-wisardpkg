# Optimized Wisardpkg

# install

```bash
git clone https://bitbucket.org/fellowshipoftheram/optimized-wisardpkg.git
pip install optimized-wisardpkg/
```

# Wisard

```python
import wisardpkg as wp
import numpy as np

X = np.array([
    [10,7,6,0,3,2,1,1,0],
    [10,7,5,8,0,2,1,3,1],
    [10,8,7,0,0,0,1,0,0],
    [10,6,5,4,0,3,2,4,0],
    [10,8,7,0,1,0,4,3,1],
    [0,2,3,1,5,7,9,10,8],
    [2,0,1,0,0,8,7,10,9],
    [1,3,1,3,1,7,6,9,8],
    [0,1,2,3,0,8,7,10,9]
],dtype="float32")
y = np.array([
    1.0,
    1.0,
    1.0,
    1.0,
    1.0,
    0.0,
    0.0,
    0.0,
    0.0
],dtype="float32")

# setup
b = wp.MeanThreshold()
entrySize = 9
tupleSize = 3
wsd = wp.Wisard(entrySize,tupleSize,binarizer=b)
bb = wp.Bleaching(confidence=1) # this is the default algorithm
wsd.setClassificationMethod(bb)

# train
wsd.train(X,y)

# classify
y_pred = wsd.classify(X)
print(y_pred)
```

# Regression

```python
import wisardpkg as wp
import numpy as np

X = np.array([
    [10,7,6,0,3,2,1,1,0],
    [10,7,5,8,0,2,1,3,1],
    [10,8,7,0,0,0,1,0,0],
    [10,6,5,4,0,3,2,4,0],
    [10,8,7,0,1,0,4,3,1],
    [0,2,3,1,5,7,9,10,8],
    [2,0,1,0,0,8,7,10,9],
    [1,3,1,3,1,7,6,9,8],
    [0,1,2,3,0,8,7,10,9]
],dtype="float32")
y = np.array([
    1.0,
    1.4,
    0.4,
    0.7,
    0.9,
    0.6,
    0.2,
    0.1,
    0.05
],dtype="float32")

# setup
b = wp.MeanThreshold()
entrySize = 9
tupleSize = 3
rew = wp.RegressionWisard(entrySize,tupleSize,binarizer=b)
rew.setMean(wp.GeometricMean()) # SimpleMean is the default algorithm
rew.setSteps(10)

# train
rew.train(X,y)

# predict
y_pred = rew.predict(X)
print(y_pred)
```