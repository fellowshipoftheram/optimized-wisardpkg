from .load_native import *
from ctypes import ArgumentError
import numpy as np

__version__ = native_version()

class Main:
    INVALID_ARGUMENTS = "Arguments types invalids!"
    def __del__(self):
        self.close()

    def close(self):
        if self.ptr:
            self.destroy(self.ptr)
            self.ptr=None

    def validate(self):
        if getattr(self,'ptr',None) is None:
            raise RuntimeError("class point is null!")
        if getattr(self,'destroy',None) is None:
            raise RuntimeError("function destroy not found!")

class Binarizer(Main):
    pass

class DefaultCut(Binarizer):

    def __init__(self):
        self.ptr = native_bin_default_create()
        self.destroy = native_bin_default_destroy

class MeanThreshold(Binarizer):

    def __init__(self, b=None):
        if b is None:
            self.ptr = native_meanthreshold_create()
        else:
            if type(b) is not int:
                raise ArgumentError(self.INVALID_ARGUMENTS)
            self.ptr = native_meanthreshold_create_b(b)
        self.destroy = native_meanthreshold_destroy

class SimpleThermometer(Binarizer):

    def __init__(self, thermometerSize, mini, maxi):
        mini = float(mini)
        maxi = float(maxi)
        if type(thermometerSize) is not int or type(mini) is not float or type(maxi) is not float:
            raise ArgumentError(self.INVALID_ARGUMENTS)
        self.ptr = native_simple_thermometer_create_b(thermometerSize,mini,maxi)
        self.destroy = native_simple_thermometer_destroy

class DynamicThermometer(Binarizer):

    def __init__(self, thermometerSizes, minis, maxis):
        thermometerSizes = np.array(thermometerSizes, dtype=np.int64)
        minis = np.array(minis, dtype=np.float64)
        maxis = np.array(maxis, dtype=np.float64)
        self.ptr = native_dynamic_thermometer_create(thermometerSizes.ctypes.data, minis.ctypes.data, maxis.ctypes.data, maxis.size)
        self.destroy = native_dynamic_thermometer_destroy

class ClassificationMethod(Main):
    pass

class Bleaching(ClassificationMethod):

    def __init__(self, activate=True, confidence=None):
        if confidence is None:
            if type(activate) is not bool:
                raise ArgumentError(self.INVALID_ARGUMENTS)
            self.ptr = native_bleaching_create_a(activate)
        else:
            if type(confidence) is not int:
                raise ArgumentError(self.INVALID_ARGUMENTS)
            self.ptr = native_bleaching_create_b(confidence)
        self.destroy = native_bleaching_destroy

class Model(Main):
    def validate(self):
        super().validate()
        if getattr(self,'native_train',None) is None:
            raise RuntimeError("function fitter not found!")
        if getattr(self,'native_train_uint8',None) is None:
            raise RuntimeError("function fitter uint8 not found!")

    def fit(self, X, y):
        self.validate()
        if not (X.dtype == np.float32 or X.dtype == np.uint8) or y.dtype != np.float32:
            raise ArgumentError(self.INVALID_ARGUMENTS)
        if X.dtype == np.float32:
            self.native_train(self.ptr, X.ctypes.data, y.ctypes.data, y.size)
        else:
            self.native_train_uint8(self.ptr, X.ctypes.data, y.ctypes.data, y.size)
    
    def train(self, X, y):
        self.fit(X, y)
    

class ClassificationModel(Model):
    def validate(self):
        super().validate()
        if getattr(self,'native_predict',None) is None:
            raise RuntimeError("function native_predict not found!")
        if getattr(self,'native_predict_uint8',None) is None:
            raise RuntimeError("function native_predict_uint8 not found!")
        if getattr(self,'native_classify',None) is None:
            raise RuntimeError("function native_classify not found!")
        if getattr(self,'native_classify_uint8',None) is None:
            raise RuntimeError("function native_classify_uint8 not found!")
        if getattr(self,'native_classes',None) is None :
            raise RuntimeError("function native_classes not found!")


    def classify(self, X):
        self.validate()
        if not (X.dtype == np.float32 or X.dtype == np.uint8):
            raise ArgumentError(self.INVALID_ARGUMENTS)
        y = np.zeros(X.shape[0],dtype=np.float32)
        if X.dtype == np.float32:
            self.native_classify(self.ptr, X.ctypes.data, y.ctypes.data, y.size)
        else:
            self.native_classify_uint8(self.ptr, X.ctypes.data, y.ctypes.data, y.size)
        return y

    def predict(self, X):
        self.validate()
        if not (X.dtype == np.float32 or X.dtype == np.uint8):
            raise ArgumentError(self.INVALID_ARGUMENTS)
        y = np.zeros(X.shape[0]*self.native_classes(self.ptr),dtype=np.float32)
        if X.dtype == np.float32:
            self.native_predict(self.ptr, X.ctypes.data, y.ctypes.data, X.shape[0])
        else:
            self.native_predict_uint8(self.ptr, X.ctypes.data, y.ctypes.data, X.shape[0])
        return np.reshape(y,(X.shape[0],self.native_classes(self.ptr)))

class RegressionModel(Model):
    def validate(self):
        super().validate()
        if getattr(self,'native_predict',None) is None:
            raise RuntimeError("function native_predict not found!")
        if getattr(self,'native_predict_uint8',None) is None:
            raise RuntimeError("function native_predict_uint8 not found!")

    def predict(self, X):
        self.validate()
        if not (X.dtype == np.float32 or X.dtype == np.uint8):
            raise ArgumentError(self.INVALID_ARGUMENTS)
        y = np.zeros(X.shape[0],dtype=np.float32)
        if X.dtype == np.float32:
            self.native_predict(self.ptr, X.ctypes.data, y.ctypes.data, y.size)
        else:
            self.native_predict_uint8(self.ptr, X.ctypes.data, y.ctypes.data, y.size)
        return y

class Wisard(ClassificationModel):
    def __init__(self, entrySize, tupleSize, binarizer=None):
        if type(entrySize) is not int or type(tupleSize) is not int:
            raise ArgumentError(self.INVALID_ARGUMENTS)
        if binarizer is None:
            binarizer = DefaultCut()
        if not isinstance(binarizer,Binarizer):
            raise ArgumentError(self.INVALID_ARGUMENTS)
        self.ptr = native_wisard_create(entrySize,tupleSize,binarizer.ptr)
        self.destroy = native_wisard_destroy
        self.native_train = native_wisard_train
        self.native_train_uint8 = native_wisard_train_uint8
        self.native_classify = native_wisard_classify
        self.native_classify_uint8 = native_wisard_classify_uint8
        self.native_predict = native_wisard_predict
        self.native_predict_uint8 = native_wisard_predict_uint8
        self.native_classes = native_wisard_get_total_classes

    def setClassificationMethod(self, clas_mth):
        self.validate()
        if not isinstance(clas_mth,ClassificationMethod):
            raise ArgumentError(self.INVALID_ARGUMENTS)
        native_wisard_set_classification_method(self.ptr, clas_mth.ptr)


class Mean(Main):
    pass

class SimpleMean(Mean):
    def __init__(self):
        self.ptr = native_simple_mean_create()
        self.destroy = native_simple_mean_destroy

class PowerMean(Mean):
    def __init__(self, p):
        if type(p) is not int:
            raise ArgumentError(self.INVALID_ARGUMENTS)
        self.ptr = native_power_mean_create(p)
        self.destroy = native_power_mean_destroy

class Median(Mean):
    def __init__(self):
        self.ptr = native_median_create()
        self.destroy = native_median_destroy

class HarmonicMean(Mean):
    def __init__(self):
        self.ptr = native_harmonic_mean_create()
        self.destroy = native_harmonic_mean_destroy

class HarmonicPowerMean(Mean):
    def __init__(self, p):
        if type(p) is not int:
            raise ArgumentError(self.INVALID_ARGUMENTS)
        self.ptr = native_harmonic_mean_create(p)
        self.destroy = native_harmonic_mean_destroy

class GeometricMean(Mean):
    def __init__(self):
        self.ptr = native_geometric_mean_create()
        self.destroy = native_geometric_mean_destroy

class ExponentialMean(Mean):
    def __init__(self):
        self.ptr = native_exponential_mean_create()
        self.destroy = native_exponential_mean_destroy

class RegressionWisard(RegressionModel):
    def __init__(self, entrySize, tupleSize, binarizer=None):
        if type(entrySize) is not int or type(tupleSize) is not int:
            raise ArgumentError(self.INVALID_ARGUMENTS)
        if binarizer is None:
            binarizer = DefaultCut()
        if not isinstance(binarizer,Binarizer):
            raise ArgumentError(self.INVALID_ARGUMENTS)
        self.ptr = native_rew_create(entrySize,tupleSize,binarizer.ptr)
        self.destroy = native_rew_destroy
        self.native_train = native_rew_train
        self.native_train_uint8 = native_rew_train_uint8
        self.native_predict = native_rew_predict
        self.native_predict_uint8 = native_rew_predict_uint8
    
    def setSteps(self, steps):
        self.validate()
        if type(steps) is not int:
            raise ArgumentError(self.INVALID_ARGUMENTS)
        native_rew_setsteps(self.ptr, steps)
    
    def setMean(self, mean):
        self.validate()
        if not isinstance(mean,Mean):
            raise ArgumentError(self.INVALID_ARGUMENTS)
        native_rew_setmean(self.ptr, mean.ptr)
