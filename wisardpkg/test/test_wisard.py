from unittest import TestCase, main
import wisardpkg as wp
import numpy as np

class WisardTestCase(TestCase):
    y = None
    X = None

    def setUp(self):
        self.X = np.array([
            [1,1,1,0,0,0,0,0,0],
            [1,1,1,1,0,0,1,0,0],
            [1,1,1,0,0,0,1,0,0],
            [1,1,1,1,0,0,0,0,0],
            [1,0,1,0,1,0,1,0,1],
            [0,0,0,0,1,1,1,1,1],
            [0,0,1,0,0,1,1,1,1],
            [0,0,1,0,1,1,1,1,1],
            [0,0,0,0,0,1,1,1,1]
        ],dtype="float32")
        self.y = np.array([
            1.0,
            1.0,
            1.0,
            1.0,
            1.0,
            0.0,
            0.0,
            0.0,
            0.0
        ],dtype="float32")

    def test_build(self):
        try:
            wsd = wp.Wisard(self.X.shape[1],3)
            self.assertIsInstance(wsd,wp.Wisard)
        except TypeError:
            self.fail("Wisard instantiation failed!")

    def test_train(self):
        try:
            wsd = wp.Wisard(self.X.shape[1],3)
            wsd.train(self.X,self.y)
        except RuntimeError and TypeError:
            self.fail("train test fail")
    
    def test_meanthreshold(self):
        try:
            m = wp.MeanThreshold()
            wsd = wp.Wisard(self.X.shape[1],3,binarizer=m)
            wsd.train(self.X,self.y)
        except RuntimeError and TypeError:
            self.fail("meanthreshold test fail")
    
    def test_simplethermometer(self):
        try:
            m = wp.SimpleThermometer(3,0.0,1.0)
            wsd = wp.Wisard(self.X.shape[1],3,binarizer=m)
            wsd.train(self.X,self.y)
        except RuntimeError and TypeError:
            self.fail("simplethermometer test fail")

    def test_dynamicthermometer(self):
        try:
            m = wp.DynamicThermometer(np.ones(self.X.shape[1])*3,np.zeros(self.X.shape[1]),np.ones(self.X.shape[1]))
            wsd = wp.Wisard(self.X.shape[1],3,binarizer=m)
            wsd.train(self.X,self.y)
        except RuntimeError and TypeError:
            self.fail("simplethermometer test fail")

    def test_bleaching(self):
        try:
            wsd = wp.Wisard(self.X.shape[1],3)
            b = wp.Bleaching(confidence=2)
            wsd.setClassificationMethod(b)
            wsd.train(self.X,self.y)
            out = wsd.classify(self.X)
            self.assertSequenceEqual(self.y.tolist(),out.tolist())
        except RuntimeError and TypeError:
            self.fail("bleaching test fail")

    def test_classify(self):
        try:
            wsd = wp.Wisard(self.X.shape[1],3)
            wsd.train(self.X,self.y)
            out = wsd.classify(self.X)
            self.assertSequenceEqual(self.y.tolist(),out.tolist())
        except RuntimeError and TypeError:
            self.fail("classify test fail")

    def test_predict(self):
        try:
            wsd = wp.Wisard(self.X.shape[1],3)
            wsd.train(self.X,self.y)
            out = wsd.predict(self.X)
            self.assertEqual(2*self.X.shape[0],out.size)
        except RuntimeError and TypeError:
            self.fail("classify test fail")

if __name__ == '__main__':
    main(verbosity=2)
