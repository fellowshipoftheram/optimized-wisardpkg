from unittest import TestCase, main
import wisardpkg as wp
import numpy as np

class RegressionWisardTestCase(TestCase):
    y = None
    X = None

    def setUp(self):
        self.X = np.array([
            [1,1,1,0,0,0,0,0,0],
            [1,1,1,1,0,0,0,0,0],
            [0,0,0,0,1,1,1,1,1],
            [0,0,0,0,0,1,1,1,1]
        ],dtype="float32")
        self.y = np.array([
            0.2,
            0.3,
            0.7,
            0.8
        ],dtype="float32")

    def test_build(self):
        try:
            wsd = wp.RegressionWisard(self.X.shape[1],3)
            self.assertIsInstance(wsd,wp.RegressionWisard)
        except TypeError:
            self.fail("Regression Wisard instantiation failed!")
    
    def test_train(self):
        try:
            wsd = wp.RegressionWisard(self.X.shape[1],3)
            wsd.train(self.X,self.y)
        except RuntimeError:
            self.fail("Regression Wisard train!")
    
    def test_predict(self):
        try:
            wsd = wp.RegressionWisard(self.X.shape[1],3)
            wsd.setSteps(10)
            wsd.setMean(wp.ExponentialMean())
            wsd.train(self.X,self.y)
            y_pred = wsd.predict(self.X)
            for i in range(len(y_pred)):
                self.assertAlmostEqual(self.y[i], y_pred[i], places=2)
        except RuntimeError:
            self.fail("Regression Wisard predict!")