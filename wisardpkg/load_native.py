from ctypes import *


def get_dll():
    import os, fnmatch
    __location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))
    file_dll = os.path.join(__location__,"wisardpkg.so")
    if not os._exists(file_dll):
        for root, dirs, files in os.walk(__location__):
            for name in files:
                if fnmatch.fnmatch(name, "wisardpkg*.so"):
                    file_dll = os.path.join(root, name)
                    return file_dll
    return file_dll

flib = CDLL(get_dll())

native_version = flib.version
native_version.argtypes = []
native_version.restype = c_char_p

### Models

# Wisard
native_wisard_create = flib.wisard_create
native_wisard_create.argtypes = [c_uint64,c_uint32,c_void_p]
native_wisard_create.restype = c_void_p

native_wisard_destroy = flib.wisard_destroy
native_wisard_destroy.argtypes = [c_void_p]
native_wisard_destroy.restype = None

native_wisard_set_classification_method = flib.wisard_set_classification_method
native_wisard_set_classification_method.argtypes = [c_void_p, c_void_p]
native_wisard_set_classification_method.restype = None

native_wisard_train = flib.wisard_train
native_wisard_train.argtypes = [c_void_p, c_void_p, c_void_p, c_uint64]
native_wisard_train.restype = None

native_wisard_train_uint8 = flib.wisard_train_uint8
native_wisard_train_uint8.argtypes = [c_void_p, c_void_p, c_void_p, c_uint64]
native_wisard_train_uint8.restype = None

native_wisard_classify = flib.wisard_classify
native_wisard_classify.argtypes = [c_void_p, c_void_p, c_void_p, c_uint64]
native_wisard_classify.restype = None

native_wisard_classify_uint8 = flib.wisard_classify_uint8
native_wisard_classify_uint8.argtypes = [c_void_p, c_void_p, c_void_p, c_uint64]
native_wisard_classify_uint8.restype = None

native_wisard_predict = flib.wisard_predict
native_wisard_predict.argtypes = [c_void_p, c_void_p, c_void_p, c_uint64]
native_wisard_predict.restype = None

native_wisard_predict_uint8 = flib.wisard_predict_uint8
native_wisard_predict_uint8.argtypes = [c_void_p, c_void_p, c_void_p, c_uint64]
native_wisard_predict_uint8.restype = None

native_wisard_get_total_classes = flib.wisard_get_total_classes
native_wisard_get_total_classes.argtypes = [c_void_p]
native_wisard_get_total_classes.restype = c_uint32

# RegressionWisard
native_rew_create = flib.rew_create
native_rew_create.argtypes = [c_uint64,c_uint32,c_void_p]
native_rew_create.restype = c_void_p

native_rew_destroy = flib.rew_destroy
native_rew_destroy.argtypes = [c_void_p]
native_rew_destroy.restype = None

native_rew_setsteps = flib.rew_setsteps
native_rew_setsteps.argtypes = [c_void_p, c_uint32]
native_rew_setsteps.restype = None

native_rew_setmean = flib.rew_setmean
native_rew_setmean.argtypes = [c_void_p, c_void_p]
native_rew_setmean.restype = None

native_rew_train = flib.rew_train
native_rew_train.argtypes = [c_void_p, c_void_p, c_void_p, c_uint64]
native_rew_train.restype = None

native_rew_train_uint8 = flib.rew_train_uint8
native_rew_train_uint8.argtypes = [c_void_p, c_void_p, c_void_p, c_uint64]
native_rew_train_uint8.restype = None

native_rew_predict = flib.rew_predict
native_rew_predict.argtypes = [c_void_p, c_void_p, c_void_p, c_uint64]
native_rew_predict.restype = None

native_rew_predict_uint8 = flib.rew_predict_uint8
native_rew_predict_uint8.argtypes = [c_void_p, c_void_p, c_void_p, c_uint64]
native_rew_predict_uint8.restype = None

### Binarizers

# DefaultCut
native_bin_default_create = flib.bin_default_create
native_bin_default_create.argtypes = []
native_bin_default_create.restype = c_void_p

native_bin_default_destroy = flib.bin_default_destroy
native_bin_default_destroy.argtypes = [c_void_p]
native_bin_default_destroy.restype = None

# MeanThreshold
native_meanthreshold_create = flib.meanthreshold_create
native_meanthreshold_create.argtypes = []
native_meanthreshold_create.restype = c_void_p

native_meanthreshold_create_b = flib.meanthreshold_create_b
native_meanthreshold_create_b.argtypes = [c_uint64]
native_meanthreshold_create_b.restype = c_void_p

native_meanthreshold_destroy = flib.meanthreshold_destroy
native_meanthreshold_destroy.argtypes = [c_void_p]
native_meanthreshold_destroy.restype = None

# SimpleThermometer
native_simple_thermometer_create = flib.simple_thermometer_create
native_simple_thermometer_create.argtypes = []
native_simple_thermometer_create.restype = c_void_p

native_simple_thermometer_create_b = flib.simple_thermometer_create_b
native_simple_thermometer_create_b.argtypes = [c_uint64, c_double, c_double]
native_simple_thermometer_create_b.restype = c_void_p

native_simple_thermometer_destroy = flib.simple_thermometer_destroy
native_simple_thermometer_destroy.argtypes = [c_void_p]
native_simple_thermometer_destroy.restype = None

# DynamicThermometer
native_dynamic_thermometer_create = flib.dynamic_thermometer_create
native_dynamic_thermometer_create.argtypes = [c_void_p, c_void_p, c_void_p, c_uint64]
native_dynamic_thermometer_create.restype = c_void_p

native_dynamic_thermometer_destroy = flib.dynamic_thermometer_destroy
native_dynamic_thermometer_destroy.argtypes = [c_void_p]
native_dynamic_thermometer_destroy.restype = None


### Classification Methods

# Bleaching
native_bleaching_create = flib.bleaching_create
native_bleaching_create.argtypes = []
native_bleaching_create.restype = c_void_p

native_bleaching_create_a = flib.bleaching_create_a
native_bleaching_create_a.argtypes = [c_bool]
native_bleaching_create_a.restype = c_void_p

native_bleaching_create_b = flib.bleaching_create_b
native_bleaching_create_b.argtypes = [c_int]
native_bleaching_create_b.restype = c_void_p

native_bleaching_destroy = flib.bleaching_destroy
native_bleaching_destroy.argtypes = [c_void_p]
native_bleaching_destroy.restype = None

### Prediction Methods

# Simple mean

native_simple_mean_create = flib.simple_mean_create
native_simple_mean_create.argtypes = []
native_simple_mean_create.restype = c_void_p

native_simple_mean_destroy = flib.simple_mean_destroy
native_simple_mean_destroy.argtypes = [c_void_p]
native_simple_mean_destroy.restype = None

# Power mean

native_power_mean_create = flib.power_mean_create
native_power_mean_create.argtypes = [c_int]
native_power_mean_create.restype = c_void_p

native_power_mean_destroy = flib.power_mean_destroy
native_power_mean_destroy.argtypes = [c_void_p]
native_power_mean_destroy.restype = None

# Median

native_median_create = flib.median_create
native_median_create.argtypes = []
native_median_create.restype = c_void_p

native_median_destroy = flib.median_destroy
native_median_destroy.argtypes = [c_void_p]
native_median_destroy.restype = None

# Harmonic mean

native_harmonic_mean_create = flib.harmonic_mean_create
native_harmonic_mean_create.argtypes = []
native_harmonic_mean_create.restype = c_void_p

native_harmonic_mean_destroy = flib.harmonic_mean_destroy
native_harmonic_mean_destroy.argtypes = [c_void_p]
native_harmonic_mean_destroy.restype = None

# Harmonic power mean

native_harmonic_power_mean_create = flib.harmonic_power_mean_create
native_harmonic_power_mean_create.argtypes = [c_int]
native_harmonic_power_mean_create.restype = c_void_p

native_harmonic_power_mean_destroy = flib.harmonic_power_mean_destroy
native_harmonic_power_mean_destroy.argtypes = [c_void_p]
native_harmonic_power_mean_destroy.restype = None

# Geometric mean

native_geometric_mean_create = flib.geometric_mean_create
native_geometric_mean_create.argtypes = []
native_geometric_mean_create.restype = c_void_p

native_geometric_mean_destroy = flib.geometric_mean_destroy
native_geometric_mean_destroy.argtypes = [c_void_p]
native_geometric_mean_destroy.restype = None

# Exponential mean

native_exponential_mean_create = flib.exponential_mean_create
native_exponential_mean_create.argtypes = []
native_exponential_mean_create.restype = c_void_p

native_exponential_mean_destroy = flib.exponential_mean_destroy
native_exponential_mean_destroy.argtypes = [c_void_p]
native_exponential_mean_destroy.restype = None