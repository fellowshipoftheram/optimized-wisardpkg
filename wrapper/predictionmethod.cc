#define SIMPLE_MEAN_SELF (static_cast<SimpleMean*>(self))
#define POWER_MEAN_SELF (static_cast<PowerMean*>(self))
#define MEDIAN_SELF (static_cast<Median*>(self))
#define HARMONIC_MEAN_SELF (static_cast<HarmonicMean*>(self))
#define HARMONIC_POWER_MEAN_SELF (static_cast<HarmonicPowerMean*>(self))
#define GEOMETRIC_MEAN_SELF (static_cast<GeometricMean*>(self))
#define EXPONENTIAL_MEAN_SELF (static_cast<ExponentialMean*>(self))

extern "C" {
    // simple mean
    void* simple_mean_create(){
        return static_cast<void*>(new SimpleMean());
    }

    void simple_mean_destroy(void* self){
        delete SIMPLE_MEAN_SELF;
    }

    // power mean
    void* power_mean_create(const int p){
        return static_cast<void*>(new PowerMean(p));
    }

    void power_mean_destroy(void* self){
        delete POWER_MEAN_SELF;
    }

    // median
    void* median_create(){
        return static_cast<void*>(new Median());
    }

    void median_destroy(void* self){
        delete MEDIAN_SELF;
    }

    // harmonic mean
    void* harmonic_mean_create(){
        return static_cast<void*>(new HarmonicMean());
    }

    void harmonic_mean_destroy(void* self){
        delete HARMONIC_MEAN_SELF;
    }

    // harmonic power mean
    void* harmonic_power_mean_create(const int p){
        return static_cast<void*>(new HarmonicPowerMean(p));
    }

    void harmonic_power_mean_destroy(void* self){
        delete HARMONIC_POWER_MEAN_SELF;
    }

    // geometric mean
    void* geometric_mean_create(){
        return static_cast<void*>(new GeometricMean());
    }

    void geometric_mean_destroy(void* self){
        delete GEOMETRIC_MEAN_SELF;
    }

    // exponential mean
    void* exponential_mean_create(){
        return static_cast<void*>(new ExponentialMean());
    }

    void exponential_mean_destroy(void* self){
        delete EXPONENTIAL_MEAN_SELF;
    }

}