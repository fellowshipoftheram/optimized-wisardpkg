#define WISARD_SELF (static_cast<Wisard*>(self))
#define REW_SELF (static_cast<RegressionWisard*>(self))

extern "C"{
    // WiSARD
    void* wisard_create(const uint64_t entrySize, const uint32_t tupleSize, void* biner){
        return static_cast<void*>(new Wisard(entrySize,tupleSize, (Binarizer*)biner));
    }

    void wisard_destroy(void* self){
        delete WISARD_SELF;
    }

    void wisard_set_classification_method(void* self, void* clas_mth){
        WISARD_SELF->setClassificationMethod((ClassificationMethod*)clas_mth);
    }

    void wisard_train(void* self, void* const X, void* const y, const uint64_t samples){
        WISARD_SELF->train((float*)X,(float*)y,samples);
    }
    void wisard_train_uint8(void* self, void* const X, void* const y, const uint64_t samples){
        WISARD_SELF->train((uint8_t*)X,(float*)y,samples);
    }

    void wisard_classify(void* self, void* const X, void* const y, const uint64_t samples){
        WISARD_SELF->classify((float*)X,(float*)y,samples);
    }
    void wisard_classify_uint8(void* self, void* const X, void* const y, const uint64_t samples){
        WISARD_SELF->classify((uint8_t*)X,(float*)y,samples);
    }

    void wisard_predict(void* self, void* const X, void* const y, const uint64_t samples){
        WISARD_SELF->predict((float*)X,(float*)y,samples);
    }
    void wisard_predict_uint8(void* self, void* const X, void* const y, const uint64_t samples){
        WISARD_SELF->predict((uint8_t*)X,(float*)y,samples);
    }

    uint32_t wisard_get_total_classes(void* self){
        return WISARD_SELF->getTotalClasses();
    }

    // Regression WiSARD
    void* rew_create(uint64_t entrySize, uint32_t tupleSize, void* biner){
        return static_cast<void*>(new RegressionWisard(entrySize,tupleSize, (Binarizer*)biner));
    }

    void rew_destroy(void* self){
        delete REW_SELF;
    }

    void rew_setmean(void* self, void* m){
        REW_SELF->setMean((Mean*)m);
    }

    void rew_setsteps(void* self, const uint32_t s){
        REW_SELF->setSteps(s);
    }

    void rew_train(void* self, void* const X, void* const y, const uint64_t samples){
        REW_SELF->train((float*)X,(float*)y,samples);
    }
    void rew_train_uint8(void* self, void* const X, void* const y, const uint64_t samples){
        REW_SELF->train((uint8_t*)X,(float*)y,samples);
    }

    void rew_predict(void* self, void* const X, void* const y, const uint64_t samples){
        REW_SELF->predict((float*)X,(float*)y,samples);
    }
    void rew_predict_uint8(void* self, void* const X, void* const y, const uint64_t samples){
        REW_SELF->predict((uint8_t*)X,(float*)y,samples);
    }
}