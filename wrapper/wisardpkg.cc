#include "../src/wisardpkg.h"
#include "utils.cc"
#include "binarizer.cc"
#include "classificationmethod.cc"
#include "predictionmethod.cc"
#include "models.cc"

extern "C" {
    const char* version(){
        return __version__.c_str();
    }
}