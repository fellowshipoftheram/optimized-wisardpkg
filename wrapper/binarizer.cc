#define BIN_DEFAULT_SELF (static_cast<DefaultCut*>(self))
#define MEANTHRESHOLD_SELF (static_cast<MeanThreshold*>(self))
#define SIMPLE_THERMOMETER_SELF (static_cast<SimpleThermometer*>(self))
#define DYNAMIC_THERMOMETER_SELF (static_cast<DynamicThermometer*>(self))

extern "C"{
    // default cut
    void* bin_default_create(){
        return static_cast<void*>(new DefaultCut());
    }

    void bin_default_destroy(void* self){
        delete BIN_DEFAULT_SELF;
    }

    // mean threshold
    void* meanthreshold_create(){
        return static_cast<void*>(new MeanThreshold());
    }

    void* meanthreshold_create_b(const uint64_t b){
        return static_cast<void*>(new MeanThreshold(b));
    }

    void meanthreshold_destroy(void* self){
        delete MEANTHRESHOLD_SELF;
    }

    // simple thermometer
    void* simple_thermometer_create(){
        return static_cast<void*>(new SimpleThermometer());
    }

    void* simple_thermometer_create_b(const uint64_t thermometerSize, const double minimum, const double maximum){
        return static_cast<void*>(new SimpleThermometer(thermometerSize,minimum,maximum));
    }

    void simple_thermometer_destroy(void* self){
        delete SIMPLE_THERMOMETER_SELF;
    }

    // dynamic thermometer
    void* dynamic_thermometer_create(void* thermometerSizes, void* minimum, void* maximum, uint32_t length){
        std::vector<uint64_t> ths((uint64_t*)thermometerSizes, &((uint64_t*)thermometerSizes)[length]);
        std::vector<double> min((double*)minimum, &((double*)minimum)[length]);
        std::vector<double> max((double*)maximum, &((double*)maximum)[length]);
        return static_cast<void*>(new DynamicThermometer(ths, min, max));
    }

    void dynamic_thermometer_destroy(void* self){
        delete DYNAMIC_THERMOMETER_SELF;
    }
}