#define BLEACHING_SELF (static_cast<Bleaching*>(self))

extern "C"{
    void* bleaching_create(){
        return static_cast<void*>(new Bleaching());
    }

    void* bleaching_create_a(const bool a){
        return static_cast<void*>(new Bleaching(a));
    }

    void* bleaching_create_b(const int c){
        return static_cast<void*>(new Bleaching(c));
    }

    void bleaching_destroy(void* self){
        delete BLEACHING_SELF;
    }
}